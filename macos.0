(def unix-consts
  '((exit      33554433)   ; 0x2000001
    (read      33554435)   ; 0x2000003
    (write     33554436)   ; 0x2000004
    (mmap      33554629)   ; 0x20000c5
    (priv-anon     4098))) ; 0x1002

(def emit-kernel-exit-fn        emit-unix-kernel-exit-fn)
(def emit-kernel-type-error-fn  emit-unix-kernel-type-error-fn)
(def emit-kernel-user-error-fn  emit-unix-kernel-user-error-fn)
(def emit-kernel-arity-error-fn emit-unix-kernel-arity-error-fn)
(def emit-kernel-write-fn       emit-unix-kernel-write-fn)
(def emit-kernel-read-fn        emit-unix-kernel-read-fn)
(def emit-kernel-alloc-fn       emit-unix-kernel-alloc-fn)

(def address-origin 4294967296) ; 0x100000000
(def program-offset 360) ; <32:header-size> + 2 * <72:segment-site> + <184:thread-size>
(def program-origin (+ address-origin program-offset))

(def emit-object-file
  (fn (symbols code-size code-emitf)
    (let text-size (+ code-size program-offset)
      (do (write/bytes                       ; Header:
            '(207 250 237 254                ;   magic number
                7   0   0   1                ;   cputype    = X86_64
                3   0   0 128                ;   cpusubtype = all
                2   0   0   0                ;   filetype   = EXECUTABLE
                3   0   0   0                ;   ncmds      = 3
               72   1   0   0                ;   sizeofcmds = 2 * <72:segment-size> + <(16 + 8 * 21):thread-size>
              129   0   0   0                ;   flags      = NOUNDEFS|TWOLEVEL
                0   0   0   0                ;   <reserved>
                                             ; Load commands:
                                             ;   Load segment __PAGEZERO:
               25   0   0   0                ;     cmd      = SEGMENT_64
               72   0   0   0                ;     cmdsize  = <72:segment-size>
               95  95  80  65 71 69 90 69    ;     segname  = "__PAGEZ
               82  79   0   0  0  0  0  0    ;                 RO     "
                0   0   0   0  0  0  0  0    ;     vmaddr   = 0
                0  16   0   0  0  0  0  0    ;     vmsize   = 0x1000
                0   0   0   0  0  0  0  0    ;     phoff    = 0
                0   0   0   0  0  0  0  0    ;     phsize   = 0
                0   0   0   0                ;     maxprot  = 0
                0   0   0   0                ;     initprot = 0
                0   0   0   0                ;     nsects   = 0
                0   0   0   0                ;     flags    = 0
                                             ;   Load segment __TEXT:
               25   0   0   0                ;     cmd      = SEGMENT_64
               72   0   0   0                ;     cmdsize  = <72:segment-size>
               95  95  84  69 88 84  0  0    ;     segname  = "__TEXT
                0   0   0   0  0  0  0  0))  ;                        "
          (write/qword address-origin)       ;     vmaddr   = <program-origin>
          (write/qword (max 4096 text-size)) ;     vmsize   = <text-size>
          (write/qword 0)                    ;     phoff    = 0
          (write/qword (max 4096 text-size)) ;     phsize   = <text-size>
          (write/dword 7)                    ;     maxprot  = READ | WRITE | EXEC
          (write/dword 7)                    ;     initprot = READ | WRITE | EXEC
          (write/dword 0)                    ;     nsects   = 0
          (write/dword 2147484672)           ;     flags    = SOME_INSTRUCTIONS | PURE_INSTRUCTIONS
                                             ;   Unix thread:
          (write/dword 5)                    ;     cmd      = UNIXTHREAD
          (write/dword 184)                  ;     cmdsize  = 184
          (write/dword 4)                    ;     flavor   = X86_THREAD_STATE_64
          (write/dword 42)                   ;     count    = 42 dwords (21 registers, each 2 dwords)
          (write/zeroes 128)                 ;     rax..r15 = 0
          (write/qword program-origin)       ;     rip      = <program-origin>
          (write/zeroes 32)                  ;     rest..   = 0
          (code-emitf 0 symbols)             ;  <code>
          (write/zeroes                      ;  <padding to min 4KB>
            (if (< text-size 4096)
                (- 4096 text-size)
                0))))))

(compile-program)
