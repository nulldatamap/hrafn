# Hrafn
Hrafn is a simplistic functional programming language.

# Primer
```
;; Hrafn supports signed integers:
1 3 32
(- 0 123)               ; => -123           ;; Though no negative literal syntax
(int? 3)                ; => true           ;; Integer predicate

;; Arithmetic functions
(+ 1 4)                 ; => 5              ;; Addition
(- 2 4)                 ; => -2             ;; Subtraction
(* 2 3)                 ; => 6              ;; Multiplication
(/ 11 3)                ; => 3              ;; Division
(mod 11 3)              ; => 2              ;; Modulo

;; Binary operations
(band 233 7)            ; => 1              ;; Bitwise and
(bor 512 3)             ; => 515            ;; Bitwise or
(bneg 7)                ; => -8             ;; Bitwise negation

;; Comparisons
(< 3 2)                 ; => false          ;; Less than
(> 3 2)                 ; => true           ;; Greater than
(<= 1 0)                ; => false          ;; Less than or equal
(>= 3 3)                ; => true           ;; Greater than or equal

;; Lists
nil '() ()              ; => () () ()       ;; Nil, the empty list
'(1 2 3)                ; => (1 2 3)        ;; Quoted list
'(1 (2 3) ())           ; => (1 (2 3) ())   ;; Nested lists
(list (< 1 2)
      nil
      (+ 1 1))          ; => (true () 2)    ;; List from values
(cons 1 '(2 3))         ; => (1 2 3)        ;; List/pair constructor
(cons 1 2)              ; => (1 . 2)
(car '(1 2 3))          ; => 1              ;; List head
(cdr '(1 2 3))          ; => '(2 3)         ;; List tail
(car (cdr '(1 2 3)))    ; => 2
(nil? nil)              ; => true           ;; Nil predicate
(cons? '(1 2 3))        ; => true           ;; Cons predicate

;; Equality
(= 1 2)                 ; => false
(= 1 true)              ; => false          ;; Works between types
(= '(1 2 3)
   (list 1 2 3))        ; => true           ;; And it's structural

;; Symbols and quoting
'wow                    ; => wow            ;; Symbol
'3                      ; => 3
(quote wow)             ; => wow            ;; Same as quote syntax
(= 'x 'y)               ; => false          ;; Symbol equality
(= 'x 'x)               ; => true

;; Functions
(fn (x) (+ x 1))        ; => #fn            ;; Anonymous functions
((fn (x) (+ x 1)) 2)    ; => 3              ;; Can be directly invoked
(def average-of
  (fn (x y)
    (/ (+ x y) 2)))     ; => ()             ;; Define global names
(def my-number 3)       ; => ()
(average-of
  my-number 7)          ; => 5              ;; Invoking a user defined fn
(fun? average-of)       ; => true           ;; Function predicate
(apply average-of
  '(10 20))             ; => 15             ;; Invoke function with arg list
                                            ;; (f x y z)=(apply f (list x y z))

;; IO
(write 97)              ; a=> ()            ;; Writes a byte to stdout
(read)                  ; => <some byte>    ;; Reads a byte from stdin
                                            ;; or -1 if stdin is closed
(error 'my-error)       ; Error: my-error   ;; Raise an error

;; Language constructs
(do 1 2 3)              ; => 3              ;; Execute multiple expressions
(if (= 1 1)
    'all-good
    'wait-what)         ; => all-good       ;; If-expression
(let x 3
  (+ 1 x))              ; => 4              ;; Local variable bindings

;; Small example programs:

;; Cat program (outputs the input)
(def cat
  (fn ()
    (let x (read)
      (if (< x 0)          ;; End of stream
          nil
          (do (write x)    ;; Write the char
              (cat))))))   ;; Recurse

(cat)                      ;; Run the program

;; List length
(def length
  (fn (l)
    (if (nil? l)
        0
        (+ 1 (length (cdr l))))))
(length '(1 2 3))      ; => 3

;; Map function
(def map
  (fn (f xs)
    (if (nil? xs)
        nil
        (cons (f (car x))
              (map f (cdr xs))))))

(def inc (fn (x) (+ x 1)))
(map inc '(0 1 2 3))  ; => (1 2 3 4)


```

# Bootstrapping
_TODO_
