#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#ifdef WIN32
#include <fcntl.h>
#endif

typedef enum {
  NilT,
  ConsT,
  IntT,
  SymT,
  BlobT,
  FunT,
} Type;

typedef struct {
  Type type;
  void* value;
} Value;


typedef struct {
  size_t size;
  uint8_t* data;
} Blob;

typedef size_t Sym;

typedef struct {
  Value car;
  Value cdr;
} Cons;

void dump_stack();
void _err_stck(const char* msg, const char* func, int line) {
  printf("\nError: %s at %s:%d\n", msg, func, line);
  dump_stack();
  exit(1);
}

#define error(msg) _err_stck(msg, __func__, __LINE__)

#define assert(e) (e ? ((void)0) : error("Assertion failed `" #e "`"))
#define IS_TYPE(v, t) ((v).type == (t))
#define ASSERT_TYPE(v, t) (assert(IS_TYPE(v,t)))
#define ASSERT_LIST(v) (assert(is_list(v)))
#define is_nil(v) IS_TYPE(v, NilT)
#define is_cons(v) IS_TYPE(v, ConsT)
#define is_list(v) (is_nil(v) || is_cons(v))
#define is_int(v) IS_TYPE(v, IntT)
#define is_sym(v) IS_TYPE(v, SymT)
#define is_blob(v) IS_TYPE(v, BlobT)
#define is_fun(v) IS_TYPE(v, FunT)
#define QUOTE_LEN 2
#define DEF_LEN 3
#define LET_LEN 4
#define IF_LEN 4
#define FN_LEN 3


Cons* as_cons(Value v) {
  ASSERT_TYPE(v, ConsT);
  return (Cons*)v.value;
}

int64_t as_int(Value v) {
  ASSERT_TYPE(v, IntT);
  return (int64_t) v.value;
}

Sym as_sym(Value v) {
  ASSERT_TYPE(v, SymT);
  return (Sym) v.value;
}

Blob* as_blob(Value v) {
  ASSERT_TYPE(v, BlobT);
  return (Blob*) v.value;
}

Value vint(int64_t i) {
  Value v = { .type = IntT, .value = (void*)i };
  return v;
}

void show_value(Value, int);
void show_list(Value v, int max_depth) {

  if (max_depth <= 1 && max_depth >= 0) {
    printf("(...)");
    return;
  }

  printf("(");

  bool first = true;
  while (!is_nil(v)) {
    if (!first) {
      printf(" ");
    }
    if (!is_cons(v)) {
      printf(". ");
      show_value(v, max_depth < 0 ? -1 : max_depth - 1);
      break;
    }
    Cons* c = as_cons(v);
    show_value(c->car, max_depth < 0 ? -1 : max_depth - 1);

    first = false;
    v = c->cdr;
  }

  printf(")");
}

void show_value(Value v, int max_depth) {
  if (max_depth == 0) {
    printf("...");
    return;
  }

  Blob* b;
  switch (v.type) {
  case NilT: printf("()"); break;
  case ConsT: show_list(v, max_depth); break;
  case IntT: printf("%lld", as_int(v)); break;
  case BlobT:
    b = as_blob(v);
    printf("<<");
    for(size_t i = 0; i < b->size; i++) {
      if (i != 0) printf(" ");
      printf("%02X", b->data[i]);
    }
    printf(">>");
    break;
  case SymT: printf("%s", (char*)as_sym(v)); break;
  case FunT:
    printf("#fun<");
    Value c = {
      .type = ConsT,
      .value = (Cons*) v.value
    };
    show_value(c, max_depth < 0 ? -1 : max_depth - 1);
    printf(">");
    break;
  }
}

#define debug_value(v) show_value(v, -1)
#define debug_list(v) show_list(v, -1)

Value NIL = { .type = NilT, .value = NULL };


Value callstack;
Value symbols;

Value S_TRUE;
Value S_FALSE;
Value S_NIL;
Value S_MINUS;
Value S_QUOTE;
Value S_DEF;
Value S_LET;
Value S_DO;
Value S_IF;
Value S_FN;
Value S_NATIVE;

Value cons(Value v, Value xs) {
  Cons* c = malloc(sizeof(Cons));
  c->car = v;
  c->cdr = xs;
  Value new = { .type = ConsT, .value = c };
  return new;
}

bool equal(Value a, Value b) {
  if (a.type != b.type) return false;
  if (is_nil(a)) return true;

  if (is_blob(a)) {
    Blob* a_blob = as_blob(a);
    Blob* b_blob = as_blob(b);
    if (a_blob->size != b_blob->size) return false;
    for(size_t i = 0; i < a_blob->size; i++) {
      if (a_blob->data[i] != b_blob->data[i]) return false;
    }
    return true;
  }

  if (is_cons(a)) {
    Cons* a_cons = as_cons(a);
    Cons* b_cons = as_cons(b);
    return equal(a_cons->car, b_cons->car)
      && equal(a_cons->cdr, b_cons->cdr);
  }

  return a.value == b.value;
}

Value intern(const char* str) {
  Value head = symbols;
  while (!is_nil(head)) {
    Cons* c = as_cons(head);
    Sym sym = as_sym(c->car);
    if (strcmp((const char*)sym, str) == 0) {
      return c->car;
    }
    head = c->cdr;
  }

  Value sym = { .type = SymT, .value = (void*) str };
  symbols = cons(sym, symbols);
  return sym;
}

Value car(Value v) {
  return as_cons(v)->car;
}

Value cdr(Value v) {
  return as_cons(v)->cdr;
}

uint64_t list_length(Value v) {
  ASSERT_LIST(v);
  if (is_nil(v)) return 0;
  else return 1 + list_length(cdr(v));
}

Value rev_lookup(Value);

Value global_env;
Value local_env;

bool is_erroring = false;

void dump_stack() {
  if (is_erroring) {
    printf("Corrupt environment:\nGlobal: ");
    debug_value(global_env);
    printf("\nLocal: ");
    debug_value(local_env);
    printf("\n");
    return;
  }
  is_erroring = true;
  Value head = callstack;
  int frame_nb = 0;
  while (!is_nil(head)) {
    if (!is_cons(head)) {
      printf("Corrupt stack: ");
      debug_value(head);
      printf("\n");
      break;
    }
    printf("[%2d] ", frame_nb);
    Value named = rev_lookup(car(car(head)));
    Value args = cdr(car(head));
    show_value(named, 4);
    printf("\n");
    while (!is_nil(args)) {
      printf("   - ");
      show_value(car(args), 6);
      printf("\n");
      args = cdr(args);
    }
    printf("\n");
    head = cdr(head);
    frame_nb++;
  }
}


bool is_ident_char(char c) {
  return (c >= 'a' && c <= 'z')
    || (c >= 'A' && c <= 'Z')
    || (c >= '0' && c <= '9')
    || c == '-' || c == '='
    || c == '/' || c == '~'
    || c == '$' || c == '%'
    || c == '^' || c == '*'
    || c == '_' || c == '+'
    || c == '?' || c == '>'
    || c == '<' || c == '!';
}

FILE* file;
int pos = 0;
int ch;

int next() {
  ch = fgetc(file);
  pos++;
  // printf("%c", ch);
  return ch;
}

void skip_whitespace() {
  while (ch != EOF) {
    if (ch == ' ' || ch == '\n') {
      next();
    } else if(ch == ';') {
      while(ch != '\n' && ch != EOF) {
        next();
      }
    } else {
      break;
    }
  }
}

void parse_sym(Value* val) {
  int i = 0;
  char buf[1024];

  while (is_ident_char(ch)) {
    if (i == sizeof(buf) - 1) {
      error("Syntax error: symbol name too long");
    }
    buf[i] = ch;
    i++;
    next();
  }
  buf[i] = 0;
  char* sym_str = malloc(i + 1);
  strcpy(sym_str, buf);
  *val = intern(sym_str);
}

void parse_int(Value* val) {
  bool neg = ch == '-';
  if (neg) {
    next();
    if (ch < '0' || ch > '9') {
      ungetc(ch, file);
      ch = '-';
      parse_sym(val);
      return;
    }
  }

  uint64_t i = 0;

  while (ch != EOF) {
    i = i * 10 + (ch - '0');
    next();
    if (ch < '0' || ch > '9') break;
  }

  if (neg) {
    i = -i;
  }
  *val = vint(i);
}

bool parse_value(Value* val);

void parse_list(Value* val) {
  next();
  skip_whitespace();
  if (ch == ')') {
    *val = NIL;
    next();
    return;
  }

  *val = cons(NIL, NIL);
  Cons* c = as_cons(*val);

  while (ch != EOF) {
    parse_value(&c->car);
    skip_whitespace();
    if (ch == ')') {
      next();
      return;
    } else {
      c->cdr = cons(NIL, NIL);
      c = as_cons(c->cdr);
    }
  }

  error("Syntax error: unbalanced parenthesis");
}

void parse_quote(Value* val) {
  next();
  skip_whitespace();
  Value body = cons(NIL, NIL);
  Cons* c = as_cons(body);

  if (!parse_value(&c->car)) {
    error("Syntax error: got EOF where value was expected");
  }

  *val = cons(S_QUOTE, body);
}

bool parse_value(Value* val) {
  skip_whitespace();
  if (ch == EOF) return false;

  if (ch == '-' || (ch >= '0' && ch <= '9')) parse_int(val);
  else if (ch == '(') parse_list(val);
  else if (ch == '\'') parse_quote(val);
  else if (is_ident_char(ch)) parse_sym(val);
  else {
    printf("ch: '%c' at %d\n", ch, pos);
    error("Syntax error: invalid character");
  }

  return true;
}

void setup_symbols() {
  symbols = NIL;
  S_TRUE = intern("true");
  S_FALSE = intern("false");
  S_NIL = intern("nil");
  S_QUOTE = intern("quote");
  S_DEF = intern("def");
  S_LET = intern("let");
  S_DO = intern("do");
  S_IF = intern("if");
  S_FN = intern("fn");
  S_NATIVE = intern("$native");
}


#define _LOOKUPF(nm, key, val, nf) Value nm(Value v) { \
    Value scopes[] = {local_env, global_env};       \
    for(int i = 0; i < 2; i++) {                    \
      Value head = scopes[i];                       \
      while (!is_nil(head)) {                       \
        Cons* head_c = as_cons(head);               \
        if (!is_nil(head_c->car)) {                 \
          Cons* binding = as_cons(head_c->car);     \
          if (equal(binding->key, v)) {             \
            return binding->val;                    \
          }                                         \
        }                                           \
        head = head_c->cdr;                         \
      }                                             \
    }                                               \
    nf                                              \
    return NIL;                                     \
  }

_LOOKUPF(lookup, car, cdr, debug_value(v); error("Undefined variable");)
_LOOKUPF(rev_lookup, cdr, car, return v;)

Value intro(Value x, Value v, Value env) {
  return cons(cons(x, v), env);
}

Value eval_env(Value);

Value apply(Value fun, Value args) {
  Value old_local = local_env;
  local_env = cons(NIL, NIL);
  Cons* closure = (Cons*) fun.value;

  if (equal(closure->car, S_NATIVE)) {
    Value (*fun_ptr)(Value) = (Value (*)(Value))closure->cdr.value;
    Value result = fun_ptr(args);
    local_env = old_local;
    return result;
  } else {
    local_env = closure->car;
    Value bindings = car(closure->cdr);

    while (!is_nil(bindings)) {
      if (is_nil(args)) error("Too few arguments");
      local_env = intro(car(bindings), car(args), local_env);
      bindings = cdr(bindings);
      args = cdr(args);
    }
    if (!is_nil(args)) error("Too many arguments");

    Value result = eval_env(car(cdr(closure->cdr)));
    local_env = old_local;
    return result;
  }
}

Value eval_env(Value e) {
  if(e.type == NilT || e.type == IntT) {
    return e;
  } else if (e.type == SymT) {
    return lookup(e);
  } else if(e.type == ConsT) {
    Value f = car(e);
    if (equal(f, S_QUOTE)) {
      assert(list_length(e) == QUOTE_LEN);
      return car(cdr(e));
    } else if (equal(f, S_DEF)) {
      assert(list_length(e) == DEF_LEN);
      Value var = car(cdr(e));
      Value old_local = local_env;
      if (is_nil(local_env)) local_env = cons(NIL, NIL);
      Value val = eval_env(car(cdr(cdr(e))));
      local_env = old_local;
      if (is_nil(local_env)) {
        global_env = intro(var, val, global_env);
      } else {
        local_env = intro(var, val, local_env);
      }
      return NIL;
    } else if (equal(f, S_LET)) {
      assert(list_length(e) == LET_LEN);
      Value var = car(cdr(e));
      Value old_local = local_env;
      if (is_nil(local_env)) local_env = cons(NIL, NIL);
      Value val = eval_env(car(cdr(cdr(e))));
      local_env = intro(var, val, old_local);
      Value body = eval_env(car(cdr(cdr(cdr(e)))));
      local_env = old_local;
      return body;
    } else if (equal(f, S_DO)) {
      Value next = cdr(e);
      Value result = NIL;
      Value old_local = local_env;
      if (is_nil(local_env)) local_env = cons(NIL, NIL);
      while (!is_nil(next)) {
        result = eval_env(car(next));
        next = cdr(next);
      }
      local_env = old_local;
      return result;
    } else if (equal(f, S_IF)) {
      assert(list_length(e) == IF_LEN);
      Value old_local = local_env;
      if (is_nil(local_env)) local_env = cons(NIL, NIL);
      Value cond = eval_env(car(cdr(e)));
      Value result;
      if (equal(cond, S_FALSE)) {
        result = eval_env(car(cdr(cdr(cdr(e)))));
      } else {
        result = eval_env(car(cdr(cdr(e))));
      }
      local_env = old_local;
      return result;
    } else if (equal(f, S_FN)) {
      assert(list_length(e) == FN_LEN);
      Value closure = {
        .type = FunT,
        .value = cons(local_env, cdr(e)).value
      };
      return closure;
    } else {
      Value fun = eval_env(car(e));
      if (fun.type != FunT) {
        debug_value(fun);
        error("Uncallable value");
      }

      Value old_local = local_env;

      if (is_nil(local_env)) local_env = cons(NIL, NIL);

      Value args = NIL;
      Value foot = NIL;
      Value head = cdr(e);
      while (!is_nil(head)) {
        Value e = eval_env(car(head));
        if (is_nil(args)) {
          args = foot = cons(e, NIL);
        } else {
          as_cons(foot)->cdr = cons(e, NIL);
          foot = cdr(foot);
        }
        head = cdr(head);
      }
#ifndef NO_CALLSTACK
      Value old_callstack = callstack;
      callstack = cons(cons(fun, args), callstack);
#endif
      local_env = old_local;
      Value r = apply(fun, args);
#ifndef NO_CALLSTACK
      callstack = old_callstack;
#endif
      return r;
    }
  }
  debug_value(e);
  error("Invalid construct");
  return NIL;
}

Value __cons(Value args) {
  return cons(car(args), car(cdr(args)));
}

Value __car(Value args) {
  return car(car(args));
}

Value __cdr(Value args) {
  return cdr(car(args));
}

Value __list(Value args) {
  return args;
}

Value __apply(Value args) {
  return apply(car(args), car(cdr(args)));
}

Value __eq(Value args) {
  if (equal(car(args), car(cdr(args)))) {
    return S_TRUE;
  }
  return S_FALSE;
}

Value __neg(Value args) {
  return vint(~as_int(car(args)));
}

#define INTOP(name, op) Value name(Value args) {              \
    return vint(as_int(car(args)) op as_int(car(cdr(args)))); \
  }

#define RELOP(name, op) Value name(Value args) {                    \
    if (as_int(car(args)) op as_int(car(cdr(args)))) return S_TRUE; \
    return S_FALSE;                                                 \
  }

INTOP(__add, +)
INTOP(__sub, -)
INTOP(__mul, *)
INTOP(__div, /)
INTOP(__mod, %)
INTOP(__band, &)
INTOP(__bor, |)
RELOP(__lt, <)
RELOP(__gt, >)
RELOP(__le, <=)
RELOP(__ge, >=)

Value __is_nil(Value args) {
  if (is_nil(car(args))) return S_TRUE;
  return S_FALSE;
}

Value __is_sym(Value args) {
  if (is_sym(car(args))) return S_TRUE;
  return S_FALSE;
}

Value __is_int(Value args) {
  if (is_int(car(args))) return S_TRUE;
  return S_FALSE;
}

Value __is_cons(Value args) {
  if (is_cons(car(args))) return S_TRUE;
  return S_FALSE;
}

Value __is_fun(Value args) {
  if (is_fun(car(args))) return S_TRUE;
  return S_FALSE;
}

Value __read(Value args) {
  int c = getchar();
  if (c == EOF) return vint(-1);
  return vint(c);
}

Value __write(Value args) {
  putchar((char)as_int(car(args)));
  return NIL;
}

Value __error(Value args) {
  printf("Error: ");
  debug_value(car(args));
  printf("\n");
#ifndef NO_CALLSTACK
  dump_stack();
#endif
  exit(1);
  return NIL;
}

void n(char* sym, Value (*fun)(Value)) {
  Value fptr = {
    .type = IntT,
    .value = fun
  };
  Value f = {
    .type = FunT,
    .value = (Cons*) cons(S_NATIVE, fptr).value
  };
  global_env = intro(intern(sym), f, global_env);
}

void setup_env() {
  global_env = NIL;

  n("cons", __cons);
  n("car", __car);
  n("cdr", __cdr);
  n("list", __list);
  n("apply", __apply);
  n("=", __eq);
  n("error", __error);
  n("nil?", __is_nil);
  n("cons?", __is_cons);
  n("int?", __is_int);
  n("sym?", __is_sym);
  n("fun?", __is_fun);
  n("read", __read);
  n("write", __write);
  n("+", __add);
  n("-", __sub);
  n("*", __mul);
  n("/", __div);
  n("mod", __mod);
  n("band", __band);
  n("bor", __bor);
  n("bneg", __neg);
  n("<", __lt);
  n(">", __gt);
  n("<=", __le);
  n(">=", __ge);

  global_env = intro(S_NIL, NIL, global_env);
  global_env = intro(S_FALSE, S_FALSE, global_env);
  global_env = intro(S_TRUE, S_TRUE, global_env);
}

int main(int argc, const char* argv[]) {
  callstack = NIL;
  setup_symbols();
  setup_env();
  local_env = NIL;

  Value v = NIL;
#ifdef WIN32
  _setmode(_fileno(stdout), _O_BINARY);
#endif

  do {
    argc--;
    argv++;
    if (argc > 0) {
      file = fopen(*argv, "r");
      if (file == NULL) {
        error("Failed to open file.");
      }
    } else {
      file = stdin;
    }

    next();
    while(parse_value(&v)) {
      Value r = eval_env(v);
      if (file == stdin) {
        printf("=> ");
        debug_value(r);
        printf("\n");
      }
    }
    if (file == stdin) break;
  } while(argc > 0);

  return 0;
}
