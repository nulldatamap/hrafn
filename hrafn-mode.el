(require 'cl)

(defvar hranf-mode-hook nil)

(defvar hranf-mode-map
  (make-sparse-keymap))

(defconst hrafn-font-lock-keywords
  (list
   '("( *def +\\([0-9A-Za-z=/~$%^*_+<>!?-]+\\)" . (1 font-lock-function-name-face))
   '("( *let +\\([0-9A-Za-z=/~$%^*_+<>!?-]+\\)" . (1 font-lock-variable-name-face))
   '("'[0-9A-Za-z=/~$%^*_+<>!?-]+" . font-lock-constant-face)
   '("\\<\\(?:def\\|fn\\|if\\|let\\|mac\\|quote\\|do\\)\\>" . font-lock-keyword-face)
   '("\\<\\(?:<=\\|>=\\|apply\\|b\\(?:and\\|neg\\|or\\)\\|c\\(?:ar\\|dr\\|ons\\??\\)\\|fun\\?\\|int\\?\\|list\\|mod\\|nil\\?\\|read\\|sym\\?\\|write\\|[*+/<=>-]\\)\\>" . font-lock-builtin-face)
   '("\\<error\\>" . font-lock-warning-face)
   '("\\<\\(true\\|false\\|nil\\|eof\\|-?[0-9]+\\)\\>" . font-lock-constant-face)
   ;;'("[^']( *\\([A-Za-z=/~$%^*_+<>!?-]+\\)" . (1 font-lock-variable-name-face))
   ))

(defvar hrafn-mode-syntax-table
  (let ((st (make-syntax-table)))
    (map 'nil (lambda (x) (modify-syntax-entry x "w" st))
         '(?- ?= ?/ ?~ ?$ ?% ?^ ?* ?_ ?+ ?? ?> ?< ?!))
    (modify-syntax-entry ?\" "_" st)
    (modify-syntax-entry ?\; "<" st)
    (modify-syntax-entry ?\n ">" st)
    (modify-syntax-entry ?'  "." st)
    st))

(define-derived-mode hrafn-mode prog-mode "hrafn"
  (set (make-local-variable 'font-lock-defaults) '(hrafn-font-lock-keywords)))

(provide 'hrafn-mode)
