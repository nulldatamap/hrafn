import struct
import itertools
import sys
import os
import platform

class Var:
    def __init__(self, size, signed=False, interp=None):
        self.size = size
        self.interp = interp
        self.signed = signed

class Padding:
    pass

class Value:
    def __init__(self, raw, var, preoff=-1, postoff=-1):
        self.raw = raw
        self.var = var
        self.preoff = preoff
        self.postoff = postoff

    def __str__(self):
        s = ""
        x = self.raw
        if x < 0:
            s = "-"
            x = -x
        return "{{}}0x{{:0{}x}}".format(self.var.size).format(s, x)

program_origin = 0x100000000

system = platform.system()
if system == "Linux":
    program_offset = 120
elif system == "Windows":
    program_offset = 1024
    program_origin += 0x2000 - program_offset
else:
    program_offset = 360


def display_rel_addr(s, v):
    absaddr = program_origin + v.preoff + v.postoff + v.raw
    if s != None:
        symaddr = s.display_addr(v.preoff + v.postoff - program_offset + v.raw, exact=False)
    else:
        symaddr = None
    if symaddr != None:
        return "0x{:016x}[{}]".format(absaddr, symaddr)
    else:
        return "0x{:016x}".format(absaddr)

def display_slot(s, v):
    if v.raw < 0:
        return "loc{}".format((-v.raw)//8)
    else:
        return "arg{}".format((v.raw//8)-2)

def display_cmp_code(s, v):
    if v.raw == 124:
        return "<"
    elif v.raw == 127:
        return ">"
    elif v.raw == 126:
        return "<="
    elif v.raw == 125:
        return ">="
    else:
        return "??"

Byte = Var(1)
Word = Var(2)
Dword = Var(4)
Qword = Var(8)
Rel32 = Var(4, True, display_rel_addr)
TypeTag = Var(1, interp=lambda s, x: ["int", "cons", "sym", "fn", "nil", "false", "true"][x.raw])
Int = Var(8, True, lambda s, x: str(x.raw >> 3))
Slot = Var(4, True, interp=display_slot)
CmpCode = Var(1, interp=display_cmp_code)

patterns = {
    "imm-int": [72, 184, Int],
    "load-slot": [72, 139, 133, Slot],
    "load-capture": [72, 139, 129, Dword],
    "load-global": [72, 139, 5, Rel32],
    "type-check": [137, 195, 128, 227, 7, 128, 251, TypeTag, 116, 5, 232, Rel32],
    "push-value": [80],
    "unscratch-value": [72, 137, 195, 88],
    "call-kernel": [232, Rel32],
    "load-nil": [184, 4, 0, 0, 0],
    "load-true": [184, 6, 0, 0, 0],
    "load-false": [184, 5, 0, 0, 0],
    "enter-frame": [85, 72, 137, 229, 72, 129, 236, Dword],
    "check-arity": [72, 61, Dword, 116, 5],
    "exit-frame": [201, 195],
    "skip-data": [233, Rel32, Padding],
    "end-static-fn": [Padding, 0, 0, 0, 0, 0, 0, 0, 0, 72, 141, 29, Rel32,
                      72, 141, 5, 234, 255, 255, 255, 72, 137, 24,
                      72, 131, 200, 3],
    "alloc-fn0": [184, Dword],
    "alloc-fn1": [72, 141, 29, Dword, 72, 137, 24, 72, 137, 195],
    "capture0": [83],
    "capture1": [91, 72, 137, 131, Dword],
    "load-fn": [72, 137, 216, 72, 131, 200, 3],
    "jump-if-not-true": [60, 6, 15, 133, Dword],
    "skip": [233, Rel32],
    "store-local": [72, 137, 133, Dword],
    "define0": [235, 8],
    "define1": [0, 0, 0, 0, 0, 0, 0, 0, 72, 137, 5, 241, 255, 255, 255],
    "load-symbol": [72, 141, 5, Rel32, 12, 2],
    "call-func": [36, 248, 72, 141, 72, 8, 72, 139, 24,
                  184, Dword, 255, 211, 72, 129, 196, Dword, 89],
    "save-env": [81],
    "call-dynamic": [72, 139, 132, 36, Dword, 36, 248, 72, 141, 72, 8, 72, 139, 24, 184, Dword,
                     255, 211, 72, 129, 196, Dword, 89],
    "type-pred": [36, 7, 137, 195, 128, 227, 7, 128, 251, TypeTag, 116,
                  7, 184, 5, 0, 0, 0, 235, 5, 184, 6, 0, 0, 0],
    "car": [72, 139, 64, 255],
    "cdr": [72, 139, 64, 7],
    "neg": [72, 247, 208, 36, 248],
    "cons0": [80, 184, 16, 0, 0, 0],
    "cons1": [91, 72, 137, 88, 8, 91, 72, 137, 24, 12, 1],
    "load-secondary-arg": [91],
    "binop0": [72, 137, 199, 88],
    "binop1": [72, 137, 251],
    "add": [72, 1, 216],
    "sub": [72, 41, 216],
    "mod": [72, 153, 72, 247, 251, 72, 193, 224, 3, 72, 137, 194],
    "div": [72, 153, 72, 247, 251, 72, 193, 224, 3],
    "mul": [72, 193, 251, 3, 72, 15, 175, 195],
    "band": [72, 33, 216],
    "bor": [72, 9, 216],
    "compare": [72, 57, 248, CmpCode, 7, 184, 5, 0, 0, 0, 235, 5, 184, 6, 0, 0, 0],
    "equal0": [72, 57, 216, 116, 7],
    "equal1": [235, 5, 184, 6, 0, 0, 0],
    # ...
    "program-init": [72, 137, 229, 72, 129, 236, Dword],
}

def pattern_size(p):
    s = 0
    for x in p:
      if type(x) == Var:
          s += x.size
      else:
          s += 1
    return s

def read_var(x, bs, pre, post):
    if x.size == 1:
        fmt = "B"
    elif x.size == 2:
        fmt = "H"
    elif x.size == 4:
        fmt = "I"
    else:
        fmt = "Q"
    if x.signed:
        fmt = fmt.lower()
    (v,) = struct.unpack(fmt, bs)
    return Value(v, x, pre, post)

class Stream:
    def __init__(self, data):
        self.data = data
        self.position = 0

    def match(self):
        for ins, pattern in patterns.items():
            m = self.match_pattern(ins, pattern)
            if m is None:
                continue
            (s, m) = m
            self.position += s
            return m
        return None

    def match_pattern(self, ins, pattern):
        vs = []
        o = 0
        for x in pattern:
            start = self.position + o
            if start >= len(self.data):
                return None
            if type(x) == Var:
                if self.position + o < len(self.data):
                    vs.append(read_var(x, self.data[start:start + x.size], self.position, o + x.size))
                    o += x.size
                else:
                    return None
            elif x == Padding:
                while (self.position + o) % 8 != 0:
                    if self.data[self.position + o] != 0:
                        return None
                    o += 1
            elif x == self.data[start]:
                o += 1
            else:
                return None
        return (o, (ins, vs))

    def eof(self):
        return self.position >= len(self.data)

    def skip(self):
        x = self.data[self.position]
        self.position += 1
        return x

class Symbols:
    def __init__(self):
        self.hassyms = False
        self.symaddr = {}
        self.addrsym = {}

    def parse(self, data):
        self.hassyms = True
        self.symaddr["<toplevel>"] = 0
        self.addrsym[0] = "<toplevel>"
        while len(data) > 0:
            (n,) = struct.unpack("I", data[:4])
            sym = str(data[4:4+n], encoding="utf-8")
            (addr,) = struct.unpack("Q", data[4+n:4+n+8])
            if n == 0:
                sym = "<fn 0x{:016x}>".format(program_origin + addr)
            self.symaddr[sym] = addr
            self.addrsym[addr] = sym
            data = data[4+n+8:]
        self.addrsym = sorted(list(self.addrsym.items()))

    def display_addr(self, offset, exact=True):
        if not self.hassyms:
            return None
        if exact:
            match = next(filter(lambda x: x[0] == offset, self.addrsym), None)
        else:
            # match = next(iter(list(filter(lambda x: x[0] <= offset, self.addrsym))[-1:]), None)
            match = next(filter(lambda x: x[0] >= offset, self.addrsym), None)
        if match != None:
            if exact:
                return "{}".format(match[1])
            else:
                x = offset - match[0]
                s = "+"
                if x < 0:
                    x = -x
                    s = "-"
                return "{} {} 0x{:x}".format(match[1], s, x)


def disassemble(fn):
    with open(fn, "rb") as f:
        data = f.read()

    syms = Symbols()
    if os.path.exists(fn + ".syms"):
        with open(fn + ".syms", "rb") as f:
            syms.parse(f.read())
        for (addr, sym) in syms.addrsym:
            print("{:<32}: 0x{:016x}".format(sym, addr + program_origin + program_offset))
        print()

    s = Stream(data)
    s.position = program_offset
    data = []

    def print_data(data, o):
        extra = ""
        if len(data) == 8:
            (q,) = struct.unpack("Q", bytes(data))
            extra = "  ;; 0x{:016x}".format(q)
        print("{:016x}  #data {}{}".format(o, " ".join("{:02x}".format(x) for x in data), extra))

    while not s.eof():
        addrstr = syms.display_addr(s.position - program_offset)
        if addrstr:
            if addrstr[0] == '$':
                break
            print("{}:".format(addrstr))
        o = s.position
        m = s.match()
        if m is None:
            data.append(s.skip())
        else:
            o -= len(data)
            while len(data) > 0:
                print_data(data[:8], o + program_origin)
                o += len(data[:8])
                data = data[8:]
            (ins, argvs) = m
            args = [ins] + ["{}".format(x) for x in argvs]
            instr = "({})".format(" ".join(args))
            extra = ""
            extras = []
            for arg in argvs:
                if arg.var.interp != None:
                    pretty_var = arg.var.interp(syms, arg)
                    if pretty_var != None:
                        extras.append(pretty_var)
            if len(extras) > 0:
                extra = ";; {}".format(" ".join(extras))
            print("{:016x}  {:<31}{}".format(program_origin + o, instr, extra))

    while len(data) > 0:
        print_data(data[:8], o + program_origin)
        o += len(data[:8])
        data = data[8:]


disassemble(sys.argv[1])
