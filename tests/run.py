import subprocess
import os
import sys
import stat
import glob
import signal
import shutil
import platform


interpreter = "./hrafn-dev"
bootstrap_compiler = None
timeout = 10
verbose = True
bootstrap = False
colors = True

system = platform.system()
if system == "Linux":
    target = "macos"
elif system == "Windows":
    target = "windows"
    interpreter += ".exe"
    colors = False
else:
    target = "macos"

class CompileError(Exception):
    pass

class ExecutionError(Exception):
    def __init__(self, out, err, code):
        try:
            self.stdout = str(out, encoding="utf-8")
        except:
            self.stdout = out
        self.stderr = str(err, encoding="utf-8")
        self.returncode = code

    def __str__(self):
        if self.returncode < 0:
            m = " ({})".format(signal.Signals(abs(self.returncode)).name)
        else:
            m = ""
        return "stdout: '{}'\nstderr: '{}'\nError code: {}{}".format(self.stdout, self.stderr, self.returncode, m)


def compile(src):
    if not bootstrap:
        compiler = [interpreter, "hrafn.0", target + ".0"]
    else:
        compiler = [bootstrap_compiler]
    p = subprocess.run(compiler, stdin=open(src), capture_output=True)
    if p.returncode != 0:
        raise CompileError(p.stdout)
    prog = ".".join(src.split(".")[:-1])
    with open(prog, "wb") as f:
        f.write(p.stdout)
    st = os.stat(prog)
    os.chmod(prog, st.st_mode | stat.S_IEXEC)
    return prog

def execute(prog, input):
    p = subprocess.run([prog], input=bytes(input, encoding='utf-8'), capture_output=True)
    if p.returncode != 0:
       raise ExecutionError(p.stdout, p.stderr, p.returncode)
    return p.stdout

def interpret(src, input):
    p = subprocess.run([interpreter, src], input=bytes(input, encoding='utf-8'), capture_output=True)
    if p.returncode != 0:
        raise ExecutionError(p.stdout, p.stderr, p.returncode)
    return p.stdout

def colored(color, m):
    global colors
    if colors:
        sys.stdout.write(color + m + "\x1b[0m")
    else:
        sys.stdout.write(m)

def wr(m):
    sys.stdout.write(m)

def vb(m):
    if verbose:
        colored("\x1b[90m", m)

def err(m):
    colored("\x1b[91m", m)

def ok(m):
    colored("\x1b[32m", m)


total_tests = 0
total_failed = 0

def run_test_case(name, src, inp):
    global total_tests, total_failed
    total_tests += 1
    wr("{0} ;; ".format(name))
    sys.stdout.flush()
    try:
        prog = compile(src)
        should_fail = False
        exn = None
        try:
            exp = interpret(src, inp)
        except ExecutionError:
            should_fail = True

        failed = False
        try:
            res = execute(prog, inp)
        except ExecutionError as e:
            failed = True
            exn = e
        if should_fail:
            if not failed:
                err("Failed, expected failure but got: '{0}'\n".format(res))
                total_failed += 1
            else:
                ok("Success\n")
        elif failed:
            err('Failed: {0}\n'.format(exn))
            total_failed += 1
        elif res == exp:
            ok("Success\n")
        else:
            err("Failed expected '{0}'\nGot '{1}'\n".format(exp, res))
            total_failed += 1
    except CompileError as e:
        err("Compilattion error: " + str(e) + "\n")
        total_failed += 1


def run_oneliners():
    if len(whitelist) > 0 and "oneliners" not in whitelist:
        return

    with open("tests/oneliners.0") as f:
        lines = f.readlines()
    if not os.path.exists("./tests/gen"):
        os.mkdir("./tests/gen")
    stack = []
    i = 0
    for line in lines:
        if line[0] in [';', '\n']:
            continue
        if line[0] == '>':
            stack.append(line[1:])
            continue
        if line[0] == '<':
            for c in line[-1]:
                if c == '<':
                    stack.pop()
                if c != '\n':
                    raise RuntimeError("Unvalid pop syntax: " + line)
            continue
        inp, src = line[:-1].split("|")
        full_src = "".join(stack) + src
        src_file = "./tests/gen/oneliner_{}.0".format(i)
        with open(src_file, "w") as f:
            f.write(full_src)

        run_test_case(src, src_file, inp)
        i += 1

def run_cases():
  for case in glob.glob("./tests/*.0"):
      if case == "./tests/oneliners.0":
          continue
      name = case.split("/")[-1][:-2]

      if len(whitelist) > 0 and name not in whitelist:
          continue

      input_file = case + ".input"
      inp = ""
      if os.path.exists(input_file):
          with open(input_file) as f:
              inp = f.read()
      run_test_case(name, case, inp)

def report():
    global total_tests, total_failed
    wr("Results:\n")
    ok("  {}".format(total_tests - total_failed))
    wr("/{} suceeded\n".format(total_tests))

    if total_failed > 0:
        err("  {} failed\n".format(total_failed))
        err("Failure\n")
        exit(1)
    else:
        ok("  0 ")
        wr("failed\n")
        ok("Success\n")
        exit(0)

def build_boostrap_compiler():
    global bootstrap_compiler
    wr("Bootstrapping...\n")
    with open("./tests/gen/bootstrap.0", "w") as bs:
        with open("./hrafn.0", "r") as f:
            bs.write(f.read())
        with open("./" + target + ".0", "r") as f:
            bs.write(f.read())

    bootstrap_compiler = compile("./tests/gen/bootstrap.0")

def test_self_compile():
    global total_tests, total_failed
    wr("Self compiling with bootstrapped compiler... ")
    shutil.copy("./tests/gen/bootstrap.0", "./tests/gen/bootstrap-2.0")
    with open("./tests/gen/bootstrap.0") as f:
        x = f.read()
    with open("./tests/gen/bootstrap-2.0") as f:
        y = f.read()
    total_tests += 1
    if x != y:
        total_failed += 1
        err("Failure: Self compiled compiler differs from bootstrapped\n")
    else:
        ok("Success\n")

if not os.path.exists(interpreter):
    err("Couldn't find Hrafn interpreter")
    exit(1)

whitelist = sys.argv[1:]

run_oneliners()
run_cases()
build_boostrap_compiler()
bootstrap = True
run_oneliners()
run_cases()
test_self_compile()

report()
